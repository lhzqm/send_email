from test_all_web import 中国科学院动物研究所, \
    中国科学院植物研究所, \
    中国科学院生物物理研究所, \
    中国科学院遗传与发育生物学研究所, \
    中山大学新闻网, \
    清华大学药学院, \
    清华大学生命科学院, \
    清华大学医学院, \
    北京生命科学研究所, \
    北京大学生命科学学院, \
    中国科学院武汉病毒研究所, \
    中国科学技术大学生命科学学院, \
    华中科技大学同济医学院基础医学院, \
    上海交通大学医学院, \
    华中农业大学, \
    中山大学生命科学学院首页动图, \
    同济大学医学院, \
    复旦大学上海医学院, \
    中国医学科学院_北京协和医学院, \
    中国农业大学生物学院, \
    中国科学院生化与细胞生物学研究所, \
    中国科学院上海生命科学研究院, \
    中国科学院心理研究所, \
    中国科学院微生物研究所, \
    中国科学院北京基因组研究所, \
    中国科学院, \
    中国科学技术大学新闻网, \
    南开要闻_南开大学, \
    上海交通大学新闻学术网, \
    武大新闻网, \
    昆明理工大学灵长类转化医学研究院, \
    浙江大学生命科学研究院, 南京大学农学院
import os
import time
import datetime
from send_email import sendEmail

start_Time = time.time()
try:
    test_Time = datetime.datetime.now().strftime('%Y-%m-%d-%H:%M:%S')  # 现在
    print('v10:运行时间:{}'.format(test_Time))

    # 操作目录
    file = r'./test'

    # 创建目录
    if not os.path.exists(file):
        os.mkdir(file)

    if not os.path.exists('./test/md5.txt'):
        f = open('./test/md5.txt', "w", encoding='utf-8')
        f.close()
    # 打开新的 md5.txt
    f0 = open('./test/md5.txt', 'r', encoding='utf-8')
    old_information = f0.readlines()
    f0.close()
    # 备份 md5.txt
    old = open('./test/old_md5.txt', 'w', encoding='utf-8')

    old_last_list = []
    if len(old_information) != 0:
        for old_dit in old_information:
            old.write(old_dit)
            old_dit = eval(old_dit)
            old_last_list.append(old_dit)
        old.close()
    else:
        old_last_list = []

    # 1 中国科学院动物研究所
    test = 中国科学院动物研究所. \
        Institute_of_Zoology('http://www.ioz.ac.cn/gb2018/xwdt/kyjz/')
    test.run()
    # http://www.ioz.ac.cn/gb2018/xwdt/kyjz/index_1.html
    # http://www.ioz.ac.cn/gb2018/xwdt/kyjz/

    # 2 中国科学院植物研究所
    test = 中国科学院植物研究所. \
        Institute_of_Botany('http://www.ibcas.ac.cn/News/')
    test.run()
    # http://www.ibcas.ac.cn/News/index_1.html
    # http://www.ibcas.ac.cn/News/

    # 3 中国科学院生物物理研究所
    test = 中国科学院生物物理研究所. \
        Institute_of_Biophysics('http://www.ibp.cas.cn/kyjz/zxdt/')
    test.run()
    # http://www.ibp.cas.cn/kyjz/
    # http://www.ibp.cas.cn/kyjz/index_1.html

    # 4 中国科学院遗传与发育生物学研究所
    test = 中国科学院遗传与发育生物学研究所. \
        Institute_of_Genetic_Development('http://www.genetics.ac.cn/xwzx/kyjz/')
    test.run()
    # http://www.genetics.ac.cn/xwzx/kyjz/
    # http://www.genetics.ac.cn/xwzx/kyjz/index_1.html

    # 5 中山大学新闻网
    test = 中山大学新闻网. \
        Zhongshan_University_News_Network('http://news2.sysu.edu.cn/ky/')
    test.run()
    # http://news2.sysu.edu.cn/ky/
    # http://news2.sysu.edu.cn/ky/index1.htm

    # 6 北京大学生命科学学院
    test = 北京大学生命科学学院. \
        Peking_University_School_of_Life_Sciences('http://www.bio.pku.edu.cn/homes/Index/news/22/22.html')
    test.run()
    # http://www.bio.pku.edu.cn/homes/Index/news/22/22.html
    # http://www.bio.pku.edu.cn/homes/Index/news/22/22_2.html

    # 7 北京生命科学研究所
    test = 北京生命科学研究所. \
        Beijing_Institute_of_Life_Sciences('http://www.nibs.ac.cn/news.php?cid=4&sid=15')
    test.run()
    # http://www.nibs.ac.cn/news.php?cid=4&sid=15
    # http://www.nibs.ac.cn/news.php?cid=4&sid=15&page=2

    # 8 清华大学医学院
    test = 清华大学医学院. \
        Tsinghua_University_School_of_Medicine('http://www.med.tsinghua.edu.cn/MoreServlet?newsClass=12')
    test.run()
    # http://www.med.tsinghua.edu.cn/MoreServlet?newsClass=12
    # http://www.med.tsinghua.edu.cn/MoreServlet?rowCount=135&curPage=2

    # 9 清华大学生命科学院
    test = 清华大学生命科学院. \
        Tsinghua_University_School_of_Life_Science('http://life.tsinghua.edu.cn/publish/smkx/11191/index.html')
    test.run()
    # http://life.tsinghua.edu.cn/publish/smkx/11191/index.html
    # http://life.tsinghua.edu.cn/publish/smkx/11191/index_2.html

    # 10 清华大学药学院
    test = 清华大学药学院. \
        Tsinghua_University_School_of_Pharmacy('http://www.sps.tsinghua.edu.cn/cn/news/achievement/')
    test.run()
    # http://www.sps.tsinghua.edu.cn/cn/news/achievement/
    # http://www.sps.tsinghua.edu.cn/cn/news/achievement/2.html

    # 11 中国科学院武汉病毒研究所
    test = 中国科学院武汉病毒研究所.Virus_research_institute('http://www.whiov.cas.cn/kxyj_160249/kyjz_160280/')
    test.run()
    # http://www.whiov.cas.cn/kyjz_105338/
    # http://www.whiov.cas.cn/kyjz_105338/index_1.html

    # 12 中国科学技术大学生命科学学院
    test = 中国科学技术大学生命科学学院.College_Of_Life_Sciences('https://biox.ustc.edu.cn/632/list.htm')
    test.run()
    # https://biox.ustc.edu.cn/632/list.htm
    # https://biox.ustc.edu.cn/632/list2.htm

    # 13 华中科技大学同济医学院基础医学院
    test = 华中科技大学同济医学院基础医学院.Test('http://jcyxy.tjmu.edu.cn/kxyj/kycg.htm')
    test.run()

    # 14 上海交通大学医学院
    test = 上海交通大学医学院.Test('https://www.shsmu.edu.cn/news/kydt1.htm')
    test.run()
    # https://www.shsmu.edu.cn/news/kydt1.htm
    # http://jcyxy.tjmu.edu.cn/kxyj/kycg/1.htm

    # 15 华中农业大学
    test = 华中农业大学.Test('http://yjs.hzau.edu.cn/cxcy.htm')
    test.run()

    # http://yjs.hzau.edu.cn/cxcy.htm
    # http://yjs.hzau.edu.cn/cxcy/10.htm

    # 16 中山大学生命科学学院首页动图
    # test = 中山大学生命科学学院首页动图.Test('http://lifesciences.sysu.edu.cn/')
    # test.run()
    # http://lifesciences.sysu.edu.cn/

    # 17 同济大学医学院
    test = 同济大学医学院.Test('http://202.120.163.6/Web/News/92')
    test.run()
    # http://202.120.163.6/Web/News/92
    # http://202.120.163.6/Web/News/92?page=2

    # 18 复旦大学上海医学院
    test = 复旦大学上海医学院.Test('http://shmc.fudan.edu.cn/')
    test.run()

    # 19 中国医学科学院_北京协和医学院
    test = 中国医学科学院_北京协和医学院.Test('http://www.cams.ac.cn/blog/category/uncategorized/')
    test.run()
    # http://www.cams.ac.cn/blog/category/uncategorized/
    # http://www.cams.ac.cn/blog/category/uncategorized/page/2/

    # 20 中国农业大学生物学院
    test = 中国农业大学生物学院.Test('http://cbs.cau.edu.cn/col/col35535/index.html')
    test.run()

    # 21 中国科学院生化与细胞生物学研究所
    test = 中国科学院生化与细胞生物学研究所.Test('http://www.sibcb.ac.cn/cpRecentRes.asp?type=%BF%C6%D1%D0%BD%F8%D5%B9')
    test.run()

    # 22 中国科学院上海生命科学研究院
    # test = 中国科学院上海生命科学研究院.Test('http://www.sibs.cas.cn/xwdt/kyjz/')
    # test.run()

    # 23 中国科学院心理研究所
    test = 中国科学院心理研究所.Test('http://www.psych.ac.cn/xwzx/kyjz/')
    test.run()

    # 24 中国科学院微生物研究所
    test = 中国科学院微生物研究所.Test('http://www.im.cas.cn/xwzx2018/kyjz/')
    test.run()
    # http://www.im.cas.cn/xwzx2018/kyjz/index_1.html

    # 25 中国科学院北京基因组研究所
    test = 中国科学院北京基因组研究所.Test('http://www.big.cas.cn/xwzx/kyjz/')
    test.run()
    # http://www.big.cas.cn/xwzx/kyjz/index_1.html

    # 26 中国科学技术大学新闻网
    test = 中国科学技术大学新闻网.Test('http://news.ustc.edu.cn/8077/list1.htm')
    test.run()
    # http://news.ustc.edu.cn/8077/list2.htm

    # 南开要闻_南开大学 27
    test = 南开要闻_南开大学.Test('http://news.nankai.edu.cn/ywsd/index.shtml')
    test.run()
    # http://news.nankai.edu.cn/ywsd/system/count//0003000/000000000000/000/000/c0003000000000000000_000000455.shtml

    # 28 上海交通大学新闻学术网
    test = 上海交通大学新闻学术网.Test('https://news.sjtu.edu.cn/tsfx/index.html')
    test.run()
    # https://news.sjtu.edu.cn/tsfx/index_2.html

    # 29 武大新闻网
    test = 武大新闻网.Test('https://news.whu.edu.cn/kydt.htm')
    test.run()
    # https://news.sjtu.edu.cn/tsfx/index_2.html

    # 30 昆明理工大学灵长类转化医学研究院
    test = 昆明理工大学灵长类转化医学研究院.Test(
        'http://www.lpbr.cn/r/v1/sites/10977956/blog?expand=blogPosts&limit=null&page=1&tag=%E6%96%B0%E9%97%BB&include_long_blurb=true')
    test.run()

    # 31 中国科学院
    test = 中国科学院.Test('http://www.cas.cn/syky/index.shtml')
    test.run()
    # http://www.cas.cn/syky/index_1.shtml

    # 32 浙江大学生命科学研究院
    test = 浙江大学生命科学研究院.Test('http://lsi.zju.edu.cn/lwlb/list.htm')
    test.run()

    # 33 南京大学农学院
    test = 南京大学农学院.Test('http://nx.njau.edu.cn/index/kycg.htm')
    test.run()

    # http://lsi.zju.edu.cn/lwlb/list.htm
    f1 = open('./test/md5.txt', 'r', encoding='utf-8')
    new_information = f1.readlines()
    f1.close()

    new_information = eval(str(new_information))

    new_last_list = []
    for new_dit in new_information:
        new_dit = eval(str(new_dit))
        new_last_list.append(new_dit)

    print("old_md5:{}".format(len(old_last_list)))
    print("new_md5:{}".format(len(new_last_list)))

    f = open('./test/result.txt', 'w', encoding='utf-8')
    for i in range(len(old_last_list)):
        # print(old_last_list)
        if old_last_list[i]['title'] == new_last_list[i]['title']:
            pass
        else:
            f.write("<p><a href='{}'>".format(new_last_list[i]['url']) + new_last_list[i][
                'name'] + "<a>" + ':\t' + '\t!!!有更新!!!\t \n更新内容为:\t{}'.format(
                new_last_list[i]['title']) + "</p>" + '\n')
    f.close()

    f = open('./test/result.txt', "r", encoding='utf-8')
    content = f.read()
    f.close()

    if len(content) == 0:
        print('{}:没有更新'.format(test_Time))
        end_Time = time.time()
        print('总用时:{}'.format(float('%.2f' % (end_Time - start_Time))))
        # quit()
        exit()
    # print(content)
    # exit()
    nowTime = datetime.datetime.now().strftime('%Y-%m-%d: %H:%M:%S')  # 现在
    title = '{}:更新情况_{}_{}'.format(nowTime, len(old_last_list), len(new_last_list))  # 邮件主题
    receivers = ['<554839316@qq.com>', '<xiaowutongzhi_zqm@163.com>']  # 收件人邮箱
    # 发送邮件
    sendEmail(receivers, title, content)
    end_Time = time.time()

    print('总用时:{}'.format(float('%.2f' % (end_Time - start_Time))))

except Exception as E:
    end_Time = time.time()
    receivers = ['<554839316@qq.com>', '<xiaowutongzhi_zqm@163.com>']
    nowTime = datetime.datetime.now().strftime('%Y-%m-%d')  # 现在
    title = '{}:出现异常!'.format(nowTime)  # 邮件主题
    content = "{}".format(E)

    sendEmail(receivers, title, content)
    print('总用时:{}'.format(float('%.2f' % (end_Time - start_Time))))
