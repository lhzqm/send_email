《Plant Physiology》发表华健教授团队“Cyclic Nucleotide-Gated Ion Chann...	2020-06-18	 https://www.shsmu.edu.cn/news/../info/1164/8068.htm
《NatureGenetics》发表宋庆鑫教授：所有五个异源四倍体棉花起源被厘清	2020-04-21	 https://www.shsmu.edu.cn/news/../info/1164/7848.htm
《PNAS》 发表万建民院士团队“An R2R3 MYB transcription factor confers b...	2019-12-21	 https://www.shsmu.edu.cn/news/../info/1164/7619.htm
《New Phytologist》发表大豆团队李艳教授“Natural variation and selectio...	2019-12-02	 https://www.shsmu.edu.cn/news/../info/1164/7529.htm
《Nature Comm. 》发表万建民院士团队“Genome-wide associated study ident...	2019-11-25	 https://www.shsmu.edu.cn/news/../info/1164/7478.htm
《Plant Physiology》发表张红生教授团队“OsSYP121 Accumulates at Fungal ...	2019-11-13	 https://www.shsmu.edu.cn/news/../info/1164/7378.htm
《Bioinformatics》发表张红生教授团队“WPMIAS: Whole-degradome-based Pla...	2019-11-11	 https://www.shsmu.edu.cn/news/../info/1164/7369.htm
《Cell Research》发表万建民院士团队“A cyclic nucleotide-gated channel ...	2019-08-26	 https://www.shsmu.edu.cn/news/../info/1164/6762.htm
《Nature Genetics》期刊发表马正强教授团队“Mutation of a histidine-rich...	2019-06-18	 https://www.shsmu.edu.cn/news/../info/1164/6760.htm
《The Plant Cell》发表张红生教授团队“Rice qGL3/OsPPKL1 Functions with ...	2019-04-01	 https://www.shsmu.edu.cn/news/../info/1164/6904.htm
《The Plant Cell》发表万建民院士团队“OsSHI1 Regulates Plant Architectu...	2019-03-27	 https://www.shsmu.edu.cn/news/../info/1164/6777.htm
《Nature Plants》发表杨东雷教授“Inducible overexpression of Ideal Plan...	2019-03-20	 https://www.shsmu.edu.cn/news/../info/1164/6907.htm
