import hashlib
import os
import requests
from lxml import etree
import time

import datetime

nowTime = datetime.datetime.now().strftime('%Y-%m-%d-%H:%M:%S')


# 打印当前时间
# print(time.ctime().split(' ')[-1])

# 清华大学医学院___第一页
class Tsinghua_University_School_of_Medicine(object):
    def __init__(self, url):
        # 构建列表页面
        self.url = url
        # 构建headers
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
        }
        self.name = '清华大学医学院'

    def get_data(self, url):
        try:
            response = requests.get(url, headers=self.headers, timeout=30)
        except Exception as e:
            file = open('./test/{}.txt'.format(self.name), 'r', encoding='utf-8')
            first_line = file.readlines()[0].split('\t')[0]
            file.close()

            f9 = open('./test/md5.txt', 'a+', encoding='utf-8')
            md5 = hashlib.md5()
            md5.update(first_line.encode())
            o = md5.hexdigest()
            f9.write("{'hashlib_md5':'%s','name':'%s','url':'%s','title':'%s'}" % (
                o, self.name, self.url, first_line) + '\n')
            f9.close()
            print("{}连接超时:\t{}".format(self.name, self.url))
            return 0
        return response.content

    def parse_list_page(self, data):

        detail_list = []
        if data == 0:
            detail_list.append(0)
            return detail_list
        # 将相应内容创建成element对象
        html = etree.HTML(data)
        # 获取所有标题节点列表
        node_list = html.xpath('/html/body/div[3]/div[1]/ul/li/h4/a')
        # 获取日期节点列表
        node_date_list = html.xpath('/html/body/div[3]/div[1]/ul/li/h4/span')

        # print(len(node_list))
        # print(len(node_date_list))

        for index in range(len(node_list)):
            temp = dict()
            temp['title'] = node_list[index].xpath('./text()')[0].replace('\r\n', "").replace(' ', "")
            temp['date'] = node_date_list[index].xpath('./text()')[0]
            temp['url'] = node_list[index].xpath('./@href')[0]
            detail_list.append(temp)

        # print(detail_list)

        return detail_list

    def run(self):
        # 循环
        # 发起列表页面请求
        data = self.get_data(self.url)
        # print('*' * 10, data)

        if data != 0:

            # 解析列表页面,获取标题详情URL列表
            # 解析列表页面请求
            # self.parse_list_page(data)
            detail_list = self.parse_list_page(data)
            # print(detail_list)
            # 写文件
            f = open('./test/清华大学医学院.txt', 'w+', encoding='utf-8')
            f1 = open('./test/md5.txt', 'a+', encoding='utf-8')
            for index, detail in enumerate(detail_list):
                if index == 0:
                    md5 = hashlib.md5()
                    md5.update(detail['title'].encode())
                    o = md5.hexdigest()
                    f1.write("{'hashlib_md5':'%s','name':'清华大学医学院','url':'%s','title':'%s'}" % (
                        o, self.url, detail['title']) + '\n')

                f.write(detail['title'] + '\t')
                f.write(detail['date'] + '\t')
                url = detail['url']
                # print(url)
                # http://www.med.tsinghua.edu.cn/SingleServlet?newsId=1871
                detail = 'http://www.med.tsinghua.edu.cn/' + url
                f.writelines(detail + "\n")
            f.close()


if __name__ == '__main__':
    test = Tsinghua_University_School_of_Medicine('http://www.med.tsinghua.edu.cn/MoreServlet?newsClass=12')
    test.run()
