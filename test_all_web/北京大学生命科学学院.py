import hashlib
import requests
from lxml import etree


# 北京大学生命科学学院___第一页
class Peking_University_School_of_Life_Sciences(object):
    def __init__(self, url):
        self.url = url
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
        }
        self.name = '北京大学生命科学学院'

    def get_data(self, url):
        try:
            response = requests.get(url, headers=self.headers, timeout=15)
        except Exception as e:
            file = open('./test/{}.txt'.format(self.name), 'r', encoding='utf-8')
            first_line = file.readlines()[0].split('\t')[0]
            file.close()

            f9 = open('./test/md5.txt', 'a+', encoding='utf-8')
            md5 = hashlib.md5()
            md5.update(first_line.encode())
            o = md5.hexdigest()
            f9.write("{'hashlib_md5':'%s','name':'%s','url':'%s','title':'%s'}" % (
                o, self.name, self.url, first_line) + '\n')
            f9.close()
            print("{}连接超时:\t{}".format(self.name, self.url))
            return 0
        return response.content

    def parse_list_page(self, data):
        # 将相应内容创建成element对象
        html = etree.HTML(data)
        # 获取所有标题节点列表
        # 获取url node_url = html.xpath('/html/body/div[8]/div[2]/div/div[2]/div/ul/li/a/@href')[0]
        node_url = html.xpath('/html/body/div[8]/div[2]/div/div[2]/div/ul/li/a')
        # 获取标题 node_title = html.xpath('/html/body/div[8]/div[2]/div/div[2]/div/ul/li/a/p/text()')[0]
        node_title = html.xpath('/html/body/div[8]/div[2]/div/div[2]/div/ul/li/a/p')
        # 获取日期 node_data = html.xpath('/html/body/div[8]/div[2]/div/div[2]/div/ul/li/a/span/text()')[0]
        node_data = html.xpath('/html/body/div[8]/div[2]/div/div[2]/div/ul/li/a/span')

        url_list = []
        for node in node_url:
            url_list.append(node.xpath('./@href')[0])

        title_list = []
        for node in node_title:
            title_list.append(node.xpath('./text()')[0])

        date_list = []
        for node in node_data:
            date_list.append(node.xpath('./text()')[0])

        total_list = []
        # 生成字典
        for i in range(len(url_list)):
            temp = dict()
            temp['title'] = title_list[i]
            temp['date'] = date_list[i]
            temp['url'] = url_list[i]
            total_list.append(temp)

        return total_list

    def run(self):
        # 构建列表页面
        # 构建headers
        # 循环
        # 发起列表页面请求

        data = self.get_data(self.url)
        if data != 0:
            # with open('test.html', 'wb')as f:
            #     f.write(data)

            # 解析列表页面,获取标题详情URL列表
            self.parse_list_page(data)
            # 解析列表页面请求
            detail_list = self.parse_list_page(data)
            # print(detail_list)

            # 写文件
            f = open('./test/北京大学生命科学学院.txt', 'w+', encoding='utf-8')
            f1 = open('./test/md5.txt', 'a+', encoding='utf-8')
            for index, detail in enumerate(detail_list):
                if index == 0:
                    md5 = hashlib.md5()
                    md5.update(detail['title'].encode())
                    o = md5.hexdigest()
                    f1.write("{'hashlib_md5':'%s','name':'北京大学生命科学学院','url':'%s','title':'%s'}" % (
                        o, self.url, detail['title']) + '\n')
                f.write(detail['title'] + '\t')
                # detail_not_point = detail['url'].replace('.', '', 1)
                f.write(detail['date'] + '\t')
                detail = 'http://www.bio.pku.edu.cn' + detail['url']
                f.write(detail + "\n")
            f.close()


if __name__ == '__main__':
    test = Peking_University_School_of_Life_Sciences('http://www.bio.pku.edu.cn/homes/Index/news/22/22.html')
    test.run()
