import hashlib
import os
import re
import requests
from lxml import etree
import time


# 打印当前时间
# print(time.ctime().split(' ')[-1])


class Test(object):
    def __init__(self, url):
        # 构建列表页面
        self.url = url
        # 构建headers
        self.headers = {
            'User-Agent': 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)',
        }
        self.name = '中国农业大学生物学院'

    def get_data(self, url):
        try:
            response = requests.get(url, headers=self.headers, timeout=15)
        except Exception as e:

            file = open('./test/{}.txt'.format(self.name), 'r', encoding='utf-8')
            first_line = file.readlines()[0].split('\t')[0]
            file.close()

            f9 = open('./test/md5.txt', 'a+', encoding='utf-8')
            md5 = hashlib.md5()
            md5.update(first_line.encode())
            o = md5.hexdigest()
            f9.write("{'hashlib_md5':'%s','name':'%s','url':'%s','title':'%s'}" % (
                o, self.name, self.url, first_line) + '\n')
            f9.close()
            print("{}连接超时:\t{}".format(self.name, self.url))
            return 0
        return response.content.decode()

    # <tr><td height="32" style="padding-left:4px;FONT-size:14px;" align="left"><font style="padding:0 4px 0 4px;
    # FONT-size:14px;"><img src='/picture/0/5f14ad18a7fe4806be11219cce7223ba.jpg' align='absmiddle' border='0'>
    # </font><a style=" font-size:15px" href="/art/2019/10/11/art_35535_641020.html"
    # title="王涛/董江丽课题组在根瘤衰老调控机制研究方面取得进展" target="_blank">
    # 王涛/董江丽课题组在根瘤衰老调控机制研究方面取得进展</a></td><td width="80"
    #  class="bt_date" style="FONT-size:12px">2019-10-11</td></tr><tr><td height="1"
    # colspan=2><div style="border-top:1px dashed #cccccc;height: 1px;overflow:hidden;">
    # </div></td></tr>]]></record>
    def parse_list_page(self, data):
        # 将相应内容创建成element对象
        # html = etree.HTML(data)
        # # 获取所有标题节点列表
        # node_list = html.xpath('//*[@id="44446"]/div/table/tr/td[1]/a')
        # # 获取日期节点列表
        # node_date_list = html.xpath('//*[@id="44446"]/div/table/tr/td[2]')

        # print(len(node_list))
        # print(len(node_date_list))
        results = re.findall(
            '<a style=" font-size:15px" href="(.*?)" title="(.*?)" target="_blank">(.*?)</a>', data)
        # print(results)
        detail_list = []
        for result in results:
            url = result[0]
            title = result[-1]
            temp = dict()
            temp['title'] = title
            temp['url'] = url
            temp['date'] = url[5:url.rfind('art', 2) - 1]
            detail_list.append(temp)

        return detail_list

    def run(self):
        # 循环
        # 发起列表页面请求
        data = self.get_data(self.url)
        if data != 0:
            # 解析列表页面,获取标题详情URL列表
            # self.parse_list_page(data)
            detail_list = self.parse_list_page(data)
            # 写文件
            f = open('./test/中国农业大学生物学院.txt', 'w+', encoding='utf-8')
            f1 = open('./test/md5.txt', 'a+', encoding='utf-8')
            for index, detail in enumerate(detail_list):
                if index == 0:
                    md5 = hashlib.md5()
                    md5.update(detail['title'].encode())
                    o = md5.hexdigest()
                    f1.write("{'hashlib_md5':'%s','name':'中国农业大学生物学院','url':'%s','title':'%s'}" % (
                        o, self.url, detail['title']) + '\n')
                f.write(detail['title'] + '\t')
                f.write(detail['date'] + '\t')
                url = detail['url']

                # http://shmc.fudan.edu.cn/content/69413
                # http://cbs.cau.edu.cn/art/2019/10/11/art_35535_641020.html
                detail = "http://cbs.cau.edu.cn" + url
                # print(detail)
                f.write(detail + "\n")
            f.close()


if __name__ == '__main__':
    test = Test('http://cbs.cau.edu.cn/col/col35535/index.html')
    test.run()
