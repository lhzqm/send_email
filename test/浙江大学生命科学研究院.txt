林世贤实验室在Nature Communications发文报道嵌合体翻译系统的开发及其应用	2020-0623	 http://lsi.zju.edu.cn/2020/0623/c25160a2157639/page.htm
2020年4月27日周杰实验室在PNAS发文解析紫霉素Viomycin抑制蛋白质翻译的作用机理		 http://lsi.zju.edu.cn/_redirect?siteId=263&columnId=25160&articleId=2091152
2020年4月23日我院朱永群实验室和生科院周艳课题组联合在Molecular Cell上发表论文发现病原菌与宿主互作新机制		 http://lsi.zju.edu.cn/_redirect?siteId=263&columnId=25160&articleId=2089882
2019年12月24日汪方炜实验室在Journal of Cell Biology发文解析染色体分离的重要调控机制	2019-1224	 http://lsi.zju.edu.cn/2019/1224/c25130a1871043/page.htm
2019年12月18日郭行实验室在PNAS发文阐明Rpn1的磷酸化调控蛋白酶体的组装和功能	2019-1217	 http://lsi.zju.edu.cn/2019/1217/c25130a1828790/page.htm
2019年12月16日任艾明实验室Nature Communications报道新型核开关SAM-VI核开关三维结构及配体识别机制研究	2019-1216	 http://lsi.zju.edu.cn/2019/1216/c25130a1826507/page.htm
2019年12月12日周青课题组在Nature发文揭示RIPK1切割位点变异导致自身炎症疾病的分子机制	2019-1211	 http://lsi.zju.edu.cn/2019/1211/c25130a1815978/page.htm
2019年11月29日宋海卫实验室在Nature Communications发文阐明BaPif1解旋酶解旋双链DNA的分子机制	2019-1128	 http://lsi.zju.edu.cn/2019/1128/c25130a1776910/page.htm
2019年11月27日汪方炜实验室在The EMBO Journal发文揭示有丝分裂期基因组稳定性的重要保护机制	2019-1127	 http://lsi.zju.edu.cn/2019/1127/c25130a1776420/page.htm
2019年11月1日靳津实验室在Cell发文阐明CD4+ T细胞嘌呤代谢紊乱如何导致焦虑症状	2019-1030	 http://lsi.zju.edu.cn/2019/1030/c25130a1733252/page.htm
2019年10月10日范衡宇实验室在Nucleic Acids Research报道ZAR1和ZAR2在卵母细胞中对母源转录组稳定性和mRNA翻译激活的调节机制	2019-1014	 http://lsi.zju.edu.cn/2019/1014/c25130a1714636/page.htm
2019年10月9日何向伟实验室在PNAS发长文阐明着丝粒重定位导致倒转减数分裂和生殖隔离	2019-1011	 http://lsi.zju.edu.cn/2019/1011/c25130a1710093/page.htm
2019年9月18日佟超实验室在Science Advances发文揭示线粒体形态和组织稳态维持的分子机制	2019-0918	 http://lsi.zju.edu.cn/2019/0918/c25130a1676851/page.htm
2019年9月6日范衡宇课题组在Cellular and Molecular Life Sciences上发表论文，揭示DCAF13调控卵母细胞成熟的分子机制	2019-1017	 http://lsi.zju.edu.cn/2019/1017/c25160a1721746/page.htm
2019年7月22日徐平龙实验室在Nature Cell Biology发文揭示经典致癌通路HER2-AKT的天然免疫识别调控以及肿瘤免疫、感染免疫和细胞衰老功能	2019-0716	 http://lsi.zju.edu.cn/2019/0716/c25130a1300386/page.htm
2019年6月14日冯新华实验室在EMBO Journal发文揭示PTPN3调控TGF-β抑癌信号的分子机制与功能	2019-0616	 http://lsi.zju.edu.cn/2019/0616/c25130a1255424/page.htm
2019年5月28日靳津实验室在Journal of Clinical Investigation发文阐明USP16调控成熟T细胞活化的关键作用与机制		 http://lsi.zju.edu.cn/_redirect?siteId=263&columnId=25160&articleId=1968928
2019年5月20日朱永群实验室在Nature Microbiology上发表论文发现特异地切割线性泛素链的全新去泛素化酶	2019-0521	 http://lsi.zju.edu.cn/2019/0521/c25130a1202059/page.htm
2019年5月14日任艾明实验室PNAS长文报道新型核酶Hatchet三维结构及催化机制研究		 http://lsi.zju.edu.cn/_redirect?siteId=263&columnId=25160&articleId=1968822
2019年4月1日周琦实验室在Nature Ecology and Evolution杂志发文阐明天堂鸟性染色体的进化历史	2019-0402	 http://lsi.zju.edu.cn/2019/0402/c25160a1154512/page.htm
2019年1月21日冯新华实验室在Nature Cell Biology发文阐明癌症发生的新调控机制和癌症靶向治疗的新思路	2019-0226	 http://lsi.zju.edu.cn/2019/0226/c25160a999335/page.htm
2018年12月7日姬峻芳实验室在Cancer Research杂志在线发文阐明一条为多类肝癌肿瘤干细胞阳性患者所共同具有的分子调节通路	2018-1208	 http://lsi.zju.edu.cn/2018/1208/c25160a946781/page.htm
2018年12月6日汪方炜实验室在JBC杂志发文解析“有丝分裂教父”调节染色体粘连的分子机制	2018-1208	 http://lsi.zju.edu.cn/2018/1208/c25160a946780/page.htm
2018年12月4日王立铭实验室在Cell Discovery在线发文报道果蝇蛋白质觅食行为及其机制	2018-1205	 http://lsi.zju.edu.cn/2018/1205/c25160a946779/page.htm
2018年11月29日汪方炜实验室在JBC杂志报道有丝分裂期着丝粒组装的正反馈调控机制	2018-1130	 http://lsi.zju.edu.cn/2018/1130/c25160a946778/page.htm
2018年11月21日张龙实验室在Cell Host & Microbe发表论文揭示调节固有免疫自激活的重要分子机制	2018-1122	 http://lsi.zju.edu.cn/2018/1122/c25160a946777/page.htm
2018年11月21日张龙实验室在Molecular Cell报道OTUB2独立于Hippo-信号直接激活YAP/TAZ并促进肿瘤转移的机制	2018-1122	 http://lsi.zju.edu.cn/2018/1122/c25160a946776/page.htm
2018年11月5日宋海实验室在EMBO Journal发文阐明VGLL4缺失抑制PD-L1的表达并参与抗肿瘤免疫调控的作用机制	2018-1106	 http://lsi.zju.edu.cn/2018/1106/c25160a946775/page.htm
2018年11月3日周琦实验室在Molecular Biology and Evolution报道新基因在其生命周期中调控方式的动态演化史	2018-1105	 http://lsi.zju.edu.cn/2018/1105/c25160a946774/page.htm
2018年11月1日宋海实验室在Nature Communications 在线发文报道YAP/TAZ 在骨关节炎中的作用机制	2018-1106	 http://lsi.zju.edu.cn/2018/1106/c25160a946773/page.htm
2018年10月18日范衡宇实验室在Nucleic Acids Research报道卵母细胞中mRNA在不同发育时期的翻译调控规律	2018-1019	 http://lsi.zju.edu.cn/2018/1019/c25160a946769/page.htm
2018年10月17日贾俊岭实验室在Genome Biology报道新型单细胞RNA测序技术Holo-Seq揭示潜在肝癌肿瘤动力学模型	2018-1018	 http://lsi.zju.edu.cn/2018/1018/c25160a946767/page.htm
2018年10月3日范衡宇实验室在Cell Death & Differentiation报道核仁蛋白DCAF13在卵子发生中的功能	2018-1006	 http://lsi.zju.edu.cn/2018/1006/c25160a946765/page.htm
2018年9月21日靳津实验室在Journal of Autoimmunity在线发文报道CRL4DCAF2调控T细胞增殖周期的机制	2018-0925	 http://lsi.zju.edu.cn/2018/0925/c25160a946762/page.htm
2018年9月12日王立铭实验室在Cell Research在线发文报道果蝇中枢神经系统检测食物中氨基酸并据此调控进食的机制	2018-0913	 http://lsi.zju.edu.cn/2018/0913/c25160a946760/page.htm
2018年8月28日范衡宇实验室在Nature Communications 报道组蛋白H3K4me3修饰调控因子CFP1在卵母细胞减数分裂中的功能和调节机制	2018-0829	 http://lsi.zju.edu.cn/2018/0829/c25160a946758/page.htm
2018年8月23日叶升实验室解析人源α5β3 GABAA受体结构--浙江大学第一个以单颗粒冷冻电镜技术得到的蛋白质原子分辨率结构	2018-0823	 http://lsi.zju.edu.cn/2018/0823/c25160a946757/page.htm
2018年8月15日范衡宇实验室在EMBO Journal发表论文揭示DCAF13维持早期胚胎发育潜能的新机制	2018-0821	 http://lsi.zju.edu.cn/2018/0821/c25160a946756/page.htm
2018年7月17日靳津实验室在JEM发表论文阐明CRL4DCAF2在自身免疫病的重要作用及分子机制	2018-0723	 http://lsi.zju.edu.cn/2018/0723/c25160a946742/page.htm
2018年7月17日汪方炜实验室在Cell Reports发文揭示细胞周期的重要调控机制	2018-0717	 http://lsi.zju.edu.cn/2018/0717/c25160a946739/page.htm
2018年6月18日佟超实验室在Autophagy杂志发表研究论文揭示线粒体蛋白转运系统调控神经系统稳态的分子机制	2018-0620	 http://lsi.zju.edu.cn/2018/0620/c25160a946737/page.htm
2018年6月14日张龙实验室在Small Method发表综述总结外泌体的一系列传统及新型提取与鉴定方法	2018-0614	 http://lsi.zju.edu.cn/2018/0614/c25160a946735/page.htm
2018年6月11日叶升实验室在eLIFE发文揭示FtsZ原丝纤维间相互作用的分子机制及其在细菌细胞分裂过程中的重要作用	2018-0614	 http://lsi.zju.edu.cn/2018/0614/c25160a946733/page.htm
2018年4月17日赵斌实验室在EMBO reports发表封面文章报道Hippo信号通路促进癌症转移的新转录调控机制	2018-0606	 http://lsi.zju.edu.cn/2018/0606/c25160a946731/page.htm
2018年4月17日宋海实验室在Oncogene发表论文报道蛋白激酶MEKK2/3在FGF分子介导的Hedgehog通路抑制中起关键作用	2018-0420	 http://lsi.zju.edu.cn/2018/0420/c25160a946729/page.htm
2018年4月3日祝赛勇实验室在Nature Communications上发表论文报道人多能干细胞精准基因编辑新技术体系	2018-0403	 http://lsi.zju.edu.cn/2018/0403/c25160a946727/page.htm
2018年3月20日宋海实验室在Cell Reports发表论文阐述Lis1 / NDE1复合物在FGF信号通路中的作用及其机制	2018-0321	 http://lsi.zju.edu.cn/2018/0321/c25160a946725/page.htm
2018年2月28日汪方炜实验室在EMBO Reports等杂志连续发文系统阐述调控染色体稳定性的新机制	2018-0228	 http://lsi.zju.edu.cn/2018/0228/c25160a946720/page.htm
2018年2月23日宋海实验室在The Journal of Biological Chemistry发表论文阐述Lis1基因在脂肪肝及肝脏肿瘤发生发展中的作用	2018-0228	 http://lsi.zju.edu.cn/2018/0228/c25160a946718/page.htm
2018年1月22日张龙实验室在Nature Immunology上发表论文揭示肿瘤外泌体调控宿主固有免疫的重要作用	2018-0125	 http://lsi.zju.edu.cn/2018/0125/c25160a946717/page.htm
