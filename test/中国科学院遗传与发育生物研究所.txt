屠强研究组发表青鳉胚胎发育过程中的转录及调控动态图谱	t20200702	http://www.genetics.ac.cn/xwzx/kyjz/202007/t20200702_5615613.html
科学家应用组学驱动系统探讨COVID-19发病机制中的代谢失调	t20200702	http://www.genetics.ac.cn/xwzx/kyjz/202007/t20200702_5615342.html
焦雨铃研究组应邀在Current Opinion in Plant Biology撰写力学调控植物器官塑形综述文章	t20200701	http://www.genetics.ac.cn/xwzx/kyjz/202007/t20200701_5614336.html
高彩霞研究组建立新型可预测多核苷酸删除基因组编辑系统	t20200630	http://www.genetics.ac.cn/xwzx/kyjz/202006/t20200630_5613252.html
修复基因印记异常大幅提高动物克隆效率	t20200622	http://www.genetics.ac.cn/xwzx/kyjz/202006/t20200622_5609867.html
大豆基因组研究取得重大进展	t20200618	http://www.genetics.ac.cn/xwzx/kyjz/202006/t20200618_5608723.html
高彩霞研究组在多重基因组编辑技术方法研究中取得新进展	t20200616	http://www.genetics.ac.cn/xwzx/kyjz/202006/t20200616_5608195.html
李家洋研究组在独脚金内酯信号转导机制研究中取得突破性进展	t20200612	http://www.genetics.ac.cn/xwzx/kyjz/202006/t20200612_5604784.html
大豆产量与品质的协同调控研究取得重要进展	t20200528	http://www.genetics.ac.cn/xwzx/kyjz/202005/t20200528_5599728.html
谢旗研究组发现植物耐盐SOS途径发挥功能的必需因子	t20200523	http://www.genetics.ac.cn/xwzx/kyjz/202005/t20200523_5585098.html
独脚金内酯和Karrikin信号转导分子机制取得新进展	t20200507	http://www.genetics.ac.cn/xwzx/kyjz/202005/t20200507_5573992.html
PM2.5暴露诱发小鼠胰岛素抵抗和肝脏脂质代谢紊乱的性别差异	t20200428	http://www.genetics.ac.cn/xwzx/kyjz/202004/t20200428_5565236.html
植物激素分析平台褚金芳团队在植物激素分析技术研究中取得新进展	t20200424	http://www.genetics.ac.cn/xwzx/kyjz/202004/t20200424_5562215.html
左建儒研究组与合作者发现植物转亚硝基化酶	t20200424	http://www.genetics.ac.cn/xwzx/kyjz/202004/t20200424_5562209.html
李云海研究组与合作者发现了水稻种子大小调控的新机制	t20200422	http://www.genetics.ac.cn/xwzx/kyjz/202004/t20200422_5560091.html
李云海研究组发现了种子大小和重量调控的重要机制	t20200417	http://www.genetics.ac.cn/xwzx/kyjz/202004/t20200417_5548127.html
储成才研究员应邀在Current Opinion in Plant Biology撰写植物氮信号调控网络综述文章	t20200416	http://www.genetics.ac.cn/xwzx/kyjz/202004/t20200416_5547936.html
李云海研究组发现了器官大小调控因子DA1参与侧枝形成的新机制	t20200413	http://www.genetics.ac.cn/xwzx/kyjz/202004/t20200413_5538718.html
周俭民研究组发现特异靶向病原细菌致病力的植物天然产物并阐明其作用的分子机制	t20200409	http://www.genetics.ac.cn/xwzx/kyjz/202004/t20200409_5535850.html
焦雨铃研究组发现干细胞谱系自我维持的新机制	t20200403	http://www.genetics.ac.cn/xwzx/kyjz/202004/t20200403_5532949.html
李家洋研究组与合作者研究发现绿色革命的伴侣基因	t20200401	http://www.genetics.ac.cn/xwzx/kyjz/202004/t20200401_5522620.html
大豆驯化相关的开花适应性研究取得重要进展	t20200331	http://www.genetics.ac.cn/xwzx/kyjz/202003/t20200331_5522290.html
高彩霞等应邀在《Nature Food》杂志撰写“利用CRISPR加速作物改良”的观点文章	t20200331	http://www.genetics.ac.cn/xwzx/kyjz/202003/t20200331_5522244.html
屠强研究组开发Decode-seq方法显著提高差异表达基因分析的准确性	t20200323	http://www.genetics.ac.cn/xwzx/kyjz/202003/t20200323_5518123.html
张劲松研究组和陈受宜研究组发现GDSL家族脂酰水解酶MHZ11调控水稻根部乙烯反应	t20200320	http://www.genetics.ac.cn/xwzx/kyjz/202003/t20200320_5517879.html
高彩霞研究组建立植物基因组引导编辑技术体系“Plant Prime Editing”	t20200317	http://www.genetics.ac.cn/xwzx/kyjz/202003/t20200317_5516409.html
田志喜研究组在《分子植物》撰写关于“大豆绿色革命”的观点文章	t20200316	http://www.genetics.ac.cn/xwzx/kyjz/202003/t20200316_5516173.html
张永清研究组发现突触稳态调控的结构基础	t20200310	http://www.genetics.ac.cn/xwzx/kyjz/202003/t20200310_5509265.html
李家洋研究组应邀在《Trends in Plant Science》杂志撰写观点综述文章	t20200307	http://www.genetics.ac.cn/xwzx/kyjz/202003/t20200307_5508731.html
杨维才研究组发现转录抑制机制决定被子植物中央细胞命运	t20200307	http://www.genetics.ac.cn/xwzx/kyjz/202003/t20200307_5508729.html
