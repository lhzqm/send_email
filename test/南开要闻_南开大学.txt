【不忘初心 牢记使命】南开大学召开“不忘初心、牢记使命”主...	2019-11-05	 http://news.nankai.edu.cn/ywsd/system/2019/11/05/030036242.shtml
南开大学团队研获高性能柔性有机太阳能电池	2019-11-05	 http://news.nankai.edu.cn/ywsd/system/2019/11/05/030036240.shtml
【不忘初心 牢记使命】校领导班子开展主题教育集中学习	2019-11-05	 http://news.nankai.edu.cn/ywsd/system/2019/11/05/030036234.shtml
2019年秋季《世界经济展望》发布会暨国际经济金融形势学术研...	2019-11-05	 http://news.nankai.edu.cn/ywsd/system/2019/11/03/030036199.shtml
【不忘初心 牢记使命】校领导班子成员讲授专题党课 师生员工...	2019-11-04	 http://news.nankai.edu.cn/ywsd/system/2019/11/04/030036213.shtml
【百年校庆】周志华教授做客“百年南开大讲坛”	2019-11-04	 http://news.nankai.edu.cn/ywsd/system/2019/11/04/030036209.shtml
近150名重点中学教师齐聚南开 共同研讨数学人才贯通式培养	2019-11-03	 http://news.nankai.edu.cn/ywsd/system/2019/11/03/030036194.shtml
【不忘初心 牢记使命】学校召开“不忘初心、牢记使命”主题教...	2019-11-02	 http://news.nankai.edu.cn/ywsd/system/2019/11/02/030036191.shtml
中国共产党第十九届中央委员会第四次全体会议公报	2019-11-02	 http://news.nankai.edu.cn/ywsd/system/2019/11/02/030036185.shtml
【百年校庆】Mukesh K. Jain院士受聘南开大学客座教授并做客...	2019-11-01	 http://news.nankai.edu.cn/ywsd/system/2019/11/01/030036181.shtml
第二期天津市骨干中青年教职人员培训班开班	2019-11-01	 http://news.nankai.edu.cn/ywsd/system/2019/11/01/030036180.shtml
南开大学环境工程专业教育认证现场考查专家反馈会举行	2019-11-01	 http://news.nankai.edu.cn/ywsd/system/2019/11/01/030036183.shtml
【不忘初心 牢记使命】南开大学领导班子召开主题教育对照党章...	2019-11-01	 http://news.nankai.edu.cn/ywsd/system/2019/11/01/030036179.shtml
2019年天津高校“双一流”背景下学科建设论坛南开举行	2019-11-01	 http://news.nankai.edu.cn/ywsd/system/2019/11/01/030036178.shtml
【不忘初心 牢记使命】校领导班子交流调研成果	2019-10-31	 http://news.nankai.edu.cn/ywsd/system/2019/10/31/030036169.shtml
【不忘初心 牢记使命】曹雪涛讲授主题教育专题党课	2019-10-31	 http://news.nankai.edu.cn/ywsd/system/2019/10/31/030036166.shtml
南开大学团队抗流感病毒新药研发领域获新进展	2019-10-31	 http://news.nankai.edu.cn/ywsd/system/2019/10/31/030036168.shtml
中国大百科全书出版社社长刘国辉一行来访	2019-10-31	 http://news.nankai.edu.cn/ywsd/system/2019/10/31/030036167.shtml
天津市政协副主席李绍洪一行到访南开	2019-10-30	 http://news.nankai.edu.cn/ywsd/system/2019/10/30/030036139.shtml
《法国外交部档案馆藏中法关系史档案汇编 卷一》法国首发式举行	2019-10-30	 http://news.nankai.edu.cn/ywsd/system/2019/10/30/030036138.shtml
第三届全国高校大学生讲思政课公开课总决赛南开举行	2019-10-30	 http://news.nankai.edu.cn/ywsd/system/2019/10/30/030036140.shtml
【不忘初心 牢记使命】校领导班子进行“不忘初心、牢记使命”...	2019-10-29	 http://news.nankai.edu.cn/ywsd/system/2019/10/29/030036134.shtml
【百年校庆】洪永淼院士做客“百年南开大讲坛”	2019-10-29	 http://news.nankai.edu.cn/ywsd/system/2019/10/29/030036127.shtml
【百年校庆】郑建华院士做客“百年南开大讲坛”	2019-10-29	 http://news.nankai.edu.cn/ywsd/system/2019/10/29/030036133.shtml
我校教师获“学习习近平总书记关于扶贫工作的重要论述”主题...	2019-10-29	 http://news.nankai.edu.cn/ywsd/system/2019/10/29/030036129.shtml
庆祝叶嘉莹教授归国执教40周年暨纪念《黄河大合唱》首演80周...	2019-10-29	 http://news.nankai.edu.cn/ywsd/system/2019/10/29/030036135.shtml
【不忘初心 牢记使命】光明日报：回望百年沧桑 不忘初心依旧...	2019-10-28	 http://news.nankai.edu.cn/ywsd/system/2019/10/28/030036099.shtml
第十二届“谈家桢生命科学奖”揭晓 16位科学家获奖	2019-10-28	 http://news.nankai.edu.cn/ywsd/system/2019/10/28/030036089.shtml
南开大学环境工程专业教育认证现场考查专家见面会举行	2019-10-28	 http://news.nankai.edu.cn/ywsd/system/2019/10/28/030036090.shtml
南开团队发展合成新物质碳-芳基糖苷新策略	2019-10-28	 http://news.nankai.edu.cn/ywsd/system/2019/10/28/030036091.shtml
